package pt.uevora;

import java.lang.FdLibm.Pow;
import java.util.Scanner;

public class MyCalculator {

    public Double execute(String expression){
        String[] split = expression.split("\\+");
        String[] split2= expression.split("\\-");
        String[] split3= expression.split("\\*");
        String[] split4= expression.split("\\/");
        String[] split5= expression.split("\\**");


        if(expression.contains("+")){
            return sum(split[0], split[1]);
        }
        if (expression.contains("-")) {
            return dif(split2[0],split2[1]);
        }
        if(expression.contains("*")){
            return mult(split3[0], split3[1]);
        }
        if (expression.contains("/")) {
            return div(split4[0], split4[1]);
        }
        
        throw new IllegalArgumentException("Invalid expression!");
    }

    private Double sum(String arg1, String arg2) {
        return new Double(arg1) + new Double(arg2);
    }

    public Double dif(String arg1, String arg2){
        return new Double(arg1) - new Double(arg2);
    }

    public Double mult(String arg1, String arg2){
        return new Double(arg1) * new Double(arg2);
    }

    public Double div(String arg1, String arg2){
        return new Double(arg1) / new Double(arg2);
    }

    public static void main(String[] args) {
        System.out.println("Calculator");
        System.out.println("Enter your expression:");
        Scanner scanner = new Scanner(System.in);
        String expression = scanner.nextLine();
        scanner.close();
        MyCalculator myCalculator = new MyCalculator();
        Object result = myCalculator.execute(expression);

        System.out.println("Result :  " + result);
    }

}
