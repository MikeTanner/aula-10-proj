package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }

    @Test
    public void testDif() throws Exception{
        Double result = calculator.execute("45-15");

        assertEquals("The result of 45 - 15 is 30", 30, (Object)result);
    }

    @Test
    public void testMult() throws Exception{
        Double result = calculator.execute("2*5");

        assertEquals("The result of 2*5", 10.0, (Object)result);
    }

    @Test
    public void testDiv() throws Exception{
        Double result = calculator.execute("5/2");

        assertEquals("The division between 5 and 2 is 2.25", 2.5, (Object)result);
    }

    @Test
    public void testPow() throws Exception{
        Double result = calculator.execute("5**2");

        assertEquals("The result of 5**2 is 25", 25.0, (Object)result);
    }
}